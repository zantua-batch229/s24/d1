//console.log("Asdf")
// ES6 Update
// let, const - are ES6 update to create variables
// var was keyword used before

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
// math.pow() allows us to get the result of a number raised to the given exponent
// Math.pow(base,exponent)
// exponent operators - ** - allows us to get the result of a number raised to the given exponent. ALternative to Math.pow()

let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

// can we also get the sqaure root of a number with exponent operator?
let squareRootOf4 = 4**.5;
console.log(squareRootOf4);

// template literals
// "",'' - string literals

let word1 = "Javascript";
let word2 = "is";
let word3 = "not";
let word4 = "Java";
let sentence = word1 + " " + word2 + " " + word3 + " " + word4 + ".";
console.log(sentence);
// too much hassle

// ${} is used in template literals to embed JS expressiong and variables
// ${} - placeholder
let sentence1 = `${word1} ${word2} ${word3} ${word4}.`
console.log(sentence1);

let sentence2 = `${word2} is an OOP language`;
console.log(sentence2);

let sentence3 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence3);

let user1 = {
	name: "Michael",
	position: "Manager",
	income: 90000,
	expenses: 50000
}

console.log(`${user1.name} is a ${user1.position} with ${user1.income} income and ${user1.expenses} expenses.`);
console.log(`His current balance is ${user1.income - user1.expenses}.`);

// destructuring arrays and objects
// desctructuring will allow us to save array elements or object properties into new variables without having to create/initialize with accessing items/properties one-by-one

let array1 = ["Curry","Lillard","Paul","Irving"];

// old method
/*let player1 = array1[0]; //curry
let player2 = array1[1]; //Lillard
let player3 = array1[2]; //Paul
let player4 = array1[3]; //Irving
console.log(player1, player2, player3, player4);
*/


// new way - array destructuring - it is when you save array items into variables.
// in arrays, order matters and that goes the same for destructuring

let [player1,player2,player3,player4] = array1;
console.log(player1, player2, player3, player4);

let array2 = ["Jokic","Embiid","Towns","Davis"];

let [center1,,, center2] = array2;
console.log(center1);
console.log(center2);

// object destructuring
// allows us ti get the value of a property and save in a variable of the same name

let pokemon1 = {
	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]
}


// order does not matter in destructuring objects
// what matters are the keys/property name

let {type,level,name,moves,personality} = pokemon1;
console.log(type);
console.log(level);
console.log(name);
console.log(moves);

//destructuring in a function

function greet(object) {
	let {name} = object;
	console.log(`Hello ${name}`);
	console.log(`${name} is my friend!`);
	console.log(`Good luck ${name}!`);

}
greet(pokemon1);


// mini-activity
let product1 = {
	productName: "Safeguard Handsoap",
	description: "Liquid Handsoap by Safeguard",
	price: 25,
	isActive: true
}

let {productName,price} = product1;
console.log(`Product Name: ${productName}. Price: ${price}`);

// Arrow function - an alternative way of writing functions in JS. However there is significant pros and cons between tradtional and arrow  functions

// traditional function

function displayMsg(){
	console.log(`Hello, world!`)
}
displayMsg();

// Arrow function
const hello = () => {
	console.log(`Hello, world!`)	
}
hello();

// arrow function with parameters:

const alertUser = (userName) => {
	console.log(`This is an alert for user ${userName}`)
}
alertUser("James1991");

// we dont use let keyword to assign arrow function to avoid updating/ converting it into a variable

// difference to the traditional -
// implicit return - is the ability of an arrow function to return value without the use of return keyword

function addNum(num1, num2){
	let result = num1+num2;
	return result;
}

let sum1 = addNum(5,10);
console.log(sum1);

// Arrow function have implcit return. when an arrow function is written in one line, it can return value without return keyword

const addNum2 = (num1, num2) => num1+num2;
let sum2 = addNum2(10,20);
console.log(sum2);

// implicit return in arrow function only works if written in only one line and without {}

// if an arrow function is written with more than one line and with a { } then we will need a return keyword

const subNum = (num1, num2) => {
	return num1-num2;
}
let diff = subNum(20,10);
console.log(diff);

const subNum2 = (num1, num2) => num1-num2;
diff = subNum2(20,10);
console.log(diff);


// traditional function vs arrow function as Object Methods

let character1 = {
	name: "Cloud Strife",
	occupation: "Soldier",
	introduceName: function(){
		// in traditional function as a method
		// "this" refers to the object where method is
		console.log(`Hi I'm ${this.name}`);
	},
	introduceJob: () => {

		console.log(`My job is ${this.occupation}`); //undefined bcos
		
		// "this" in arrow function refers to global window/object or the whole document
		console.log(this);	
	}
}
character1.introduceName();
character1.introduceJob();

// Class Based Object Blueprints
	// In JS, Classes are templates of Objects
	// we can use classes to create objects following the structure of the class similar to a constructor function

	// constructor function

/*	function Pokemon(name, type,level){
		this.name = name;
		this.type = type;
		this.level = level;
	}

	let pokemon2 = new Pokemon("pika", "electric",25);
	console.log(pokemon2);*/

	// with the advent of ES6, we are now introduced to new way of creating objects with a blueprint with the use of classes

	// classes function
	// classes are used to create objects

	class Car {
		constructor(brand,model,year){
			this.brand = brand;
			this.model = model;
			this.year = year;
		}
	}

	let car1 = new Car("Toyota","Vios","2015");
	let car2 = new Car("Cooper","Mini","2005");
	let car3 = new Car("Porsche","911","2026");

	console.log(car1);
	console.log(car2);
	console.log(car3);

	// mini activity

	class Pokemon {
		constructor(name, type,level){
		this.name = name;
		this.type = type;
		this.level = level;
		}
	}

	let pokemon10 = new Pokemon("Pikachu","electric",20);
	let pokemon11 = new Pokemon("Geodude","rock",30);
	let pokemon12 = new Pokemon("Balbasaur","grass",35);

	console.log(pokemon10);
	console.log(pokemon11);
	console.log(pokemon12);	

// Arrow functions in array methods 

// traditional
let numArr = [2,10,3,33,2];
let reduceNumber = numArr.reduce(function(x,y){
	return x+y; //returns sum of all umbers in numArr
})

console.log(reduceNumber);

// reduce with arrow function

let reduceNumber2 = numArr.reduce((x,y) => {
	return x+y;
})
console.log(reduceNumber2);

// reduce with implicit return

let reduceNumber3 = numArr.reduce((x,y) => x+y);
console.log(reduceNumber3);

// traditional
let mappedNum = numArr.map(function(num){
	return num*2;
})
console.log(mappedNum);

// arrow function
let mappedNum3 = numArr.map((num) => {
	return num*2;
})
console.log(mappedNum3);


// arrow function -implicit return

let mappedNum2 = numArr.map((num) => num*2);
console.log(mappedNum2);





